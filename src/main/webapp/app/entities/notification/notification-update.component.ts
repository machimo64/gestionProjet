import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { INotification, Notification } from 'app/shared/model/notification.model';
import { NotificationService } from './notification.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { ITache } from 'app/shared/model/tache.model';
import { TacheService } from 'app/entities/tache/tache.service';
import { IListe } from 'app/shared/model/liste.model';
import { ListeService } from 'app/entities/liste/liste.service';
import { IProjet } from 'app/shared/model/projet.model';
import { ProjetService } from 'app/entities/projet/projet.service';
import { IMembres } from 'app/shared/model/membres.model';
import { MembresService } from 'app/entities/membres/membres.service';

type SelectableEntity = IUser | ITypeNotification | ITache | IListe | IProjet | IMembres;

@Component({
  selector: 'jhi-notification-update',
  templateUrl: './notification-update.component.html'
})
export class NotificationUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  typenotifications: ITypeNotification[] = [];
  taches: ITache[] = [];
  listes: IListe[] = [];
  projets: IProjet[] = [];
  membres: IMembres[] = [];

  editForm = this.fb.group({
    id: [],
    dateHeure: [null, [Validators.required]],
    emetteur: [null, Validators.required],
    destinataire: [null, Validators.required],
    typeNotification: [null, Validators.required],
    tache: [],
    liste: [],
    projet: [],
    membres: []
  });

  constructor(
    protected notificationService: NotificationService,
    protected userService: UserService,
    protected typeNotificationService: TypeNotificationService,
    protected tacheService: TacheService,
    protected listeService: ListeService,
    protected projetService: ProjetService,
    protected membresService: MembresService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ notification }) => {
      if (!notification.id) {
        const today = moment().startOf('day');
        notification.dateHeure = today;
      }

      this.updateForm(notification);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.typeNotificationService.query().subscribe((res: HttpResponse<ITypeNotification[]>) => (this.typenotifications = res.body || []));

      this.tacheService.query().subscribe((res: HttpResponse<ITache[]>) => (this.taches = res.body || []));

      this.listeService.query().subscribe((res: HttpResponse<IListe[]>) => (this.listes = res.body || []));

      this.projetService.query().subscribe((res: HttpResponse<IProjet[]>) => (this.projets = res.body || []));

      this.membresService.query().subscribe((res: HttpResponse<IMembres[]>) => (this.membres = res.body || []));
    });
  }

  updateForm(notification: INotification): void {
    this.editForm.patchValue({
      id: notification.id,
      dateHeure: notification.dateHeure ? notification.dateHeure.format(DATE_TIME_FORMAT) : null,
      emetteur: notification.emetteur,
      destinataire: notification.destinataire,
      typeNotification: notification.typeNotification,
      tache: notification.tache,
      liste: notification.liste,
      projet: notification.projet,
      membres: notification.membres
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const notification = this.createFromForm();
    if (notification.id !== undefined) {
      this.subscribeToSaveResponse(this.notificationService.update(notification));
    } else {
      this.subscribeToSaveResponse(this.notificationService.create(notification));
    }
  }

  private createFromForm(): INotification {
    return {
      ...new Notification(),
      id: this.editForm.get(['id'])!.value,
      dateHeure: this.editForm.get(['dateHeure'])!.value ? moment(this.editForm.get(['dateHeure'])!.value, DATE_TIME_FORMAT) : undefined,
      emetteur: this.editForm.get(['emetteur'])!.value,
      destinataire: this.editForm.get(['destinataire'])!.value,
      typeNotification: this.editForm.get(['typeNotification'])!.value,
      tache: this.editForm.get(['tache'])!.value,
      liste: this.editForm.get(['liste'])!.value,
      projet: this.editForm.get(['projet'])!.value,
      membres: this.editForm.get(['membres'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INotification>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
