import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { UserService } from 'app/core/user/user.service';
import { IUser } from 'app/core/user/user.model';
import { MembresService } from 'app/entities/membres/membres.service';
import { TacheService } from 'app/entities/tache/tache.service';
import { ITache } from 'app/shared/model/tache.model';
import { MatTableDataSource } from '@angular/material/table';
import { Participant } from 'app/shared/model/participant.model';
import { ParticipantService } from 'app/entities/participant/participant.service';
import { forkJoin } from 'rxjs';
import { Notification } from 'app/shared/model/notification.model';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { NotificationService } from 'app/entities/notification/notification.service';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import * as moment from 'moment';

@Component({
  templateUrl: './participant-tache-add.component.html'
})
export class ParticipantTacheAddComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['id', 'login', 'firstName', 'lastName', 'email', 'add'];
  idTache?: number;
  tache?: ITache;
  users: IUser[] = [];
  elements: IUser[] = [];
  previous: string | null = null;
  dataSource: any;

  user!: IUser;
  newUser!: IUser | undefined;
  typeNotification!: ITypeNotification;

  constructor(
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected userService: UserService,
    protected membresService: MembresService,
    protected participantService: ParticipantService,
    protected tacheService: TacheService,
    protected typeNotificationService: TypeNotificationService,
    protected notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    if (this.idTache)
      this.tacheService.find(this.idTache).subscribe(tache => {
        if (tache.body) this.tache = tache.body;
      });

    if (this.idTache)
      this.membresService.queryByNotTache(this.idTache).subscribe(membres => {
        if (membres.body)
          membres.body.forEach(membre => {
            if (membre.user) this.users.push(membre.user);
          });
        if (this.users) this.dataSource = new MatTableDataSource(this.users);
      });
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ajouterParticipant(user: IUser): void {
    if (user.login) {
      const btn = document.getElementById(user.login);
      if (btn) {
        btn.className = 'btn btn-default btn-sm';
        btn.setAttribute('disabled', 'true');
      }
    }
    const participant = new Participant(undefined, user, this.tache);
    this.newUser = participant.user;
    console.log(this.newUser);
    this.participantService.create(participant).subscribe(() => {
      this.eventManager.broadcast('participantListModification');
      this.loadAll();
    });
  }

  loadAll(): void {
    forkJoin([this.userService.getCurrentUser(), this.typeNotificationService.findByNom('Assignation d une tache à un membre')]).subscribe(
      resultat => {
        this.user = resultat[0] || null;
        console.log(this.user);
        this.typeNotification = resultat[1].body!;
        console.log(resultat[1].body!);
        this.createNotification();
      }
    );
  }

  createNotification(): void {
    const notification = new Notification(undefined, moment(), this.user, this.newUser, this.typeNotification, this.tache);
    this.notificationService.create(notification).subscribe(resultat => console.log(resultat));
  }

  ngOnDestroy(): void {}
}
