import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { IListe, Liste } from 'app/shared/model/liste.model';
import { ListeService } from '../liste/liste.service';
import { IProjet } from 'app/shared/model/projet.model';
import { Notification } from 'app/shared/model/notification.model';
import { IMembres } from 'app/shared/model/membres.model';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import { IUser } from 'app/core/user/user.model';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { UserService } from 'app/core/user/user.service';
import { MembresService } from 'app/entities/membres/membres.service';
import { NotificationService } from 'app/entities/notification/notification.service';
import * as moment from 'moment';

@Component({
  selector: 'jhi-liste-update',
  templateUrl: './projet-new-liste.component.html'
})
export class ProjetNewListeComponent implements OnInit {
  isSaving = false;
  projet: IProjet | null = null;

  membres: IMembres[] = [];
  liste: IListe | null = null;
  typeNotification!: ITypeNotification;
  user!: IUser;
  listenotification!: IListe | null;

  editForm = this.fb.group({
    titre: [null, [Validators.required]],
    projet: [null, Validators.required],
    position: [null, Validators.required]
  });

  constructor(
    protected listeService: ListeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected typeNotificationService: TypeNotificationService,
    protected userService: UserService,
    protected membresService: MembresService,
    protected notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    // On récupére le projet
    this.activatedRoute.data.subscribe(({ projet }) => {
      this.projet = projet;
    });
    this.editForm.patchValue({
      projet: this.projet
    });
    // On attribue a la liste une position, valeur aléatoire
    this.editForm.patchValue({ position: 0 });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const liste = this.createFromForm();
    this.liste = liste;
    if (liste.id !== undefined) {
      this.loadAll();
      this.subscribeToSaveResponse(this.listeService.update(liste));
    } else {
      this.loadAll();
      this.subscribeToSaveResponse(this.listeService.create(liste));
    }
  }

  loadAll(): void {
    forkJoin([
      this.membresService.queryByProjet(this.liste!.projet!.id!),
      this.userService.getCurrentUser(),
      this.typeNotificationService.findByNom('Liste créée')
    ]).subscribe(resultat => {
      this.membres = resultat[0].body || [];
      this.user = resultat[1] || null;
      console.log(this.user);
      this.typeNotification = resultat[2].body!;
      console.log(resultat[2].body!);
    });
  }

  createNotification(): void {
    this.membres.forEach(membre => {
      if (JSON.stringify(this.user) !== JSON.stringify(membre.user)) {
        const notification = new Notification(
          undefined,
          moment(),
          this.user,
          membre.user,
          this.typeNotification,
          undefined,
          this.listenotification!
        );
        this.notificationService.create(notification).subscribe(resultat => console.log(resultat));
      }
    });
  }

  private createFromForm(): IListe {
    return {
      ...new Liste(),
      titre: this.editForm.get(['titre'])!.value,
      projet: this.editForm.get(['projet'])!.value,
      position: this.editForm.get(['position'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IListe>>): void {
    result.subscribe(res => {
      this.listenotification = res.body;
      this.onSaveSuccess();
    });
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
    this.createNotification();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProjet): any {
    return item.id;
  }
}
