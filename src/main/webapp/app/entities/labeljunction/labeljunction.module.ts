import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestionSharedModule } from 'app/shared/shared.module';
import { LabeljunctionComponent } from './labeljunction.component';
import { LabeljunctionDetailComponent } from './labeljunction-detail.component';
import { LabeljunctionUpdateComponent } from './labeljunction-update.component';
import { LabeljunctionDeleteDialogComponent } from './labeljunction-delete-dialog.component';
import { labeljunctionRoute } from './labeljunction.route';

@NgModule({
  imports: [GestionSharedModule, RouterModule.forChild(labeljunctionRoute)],
  declarations: [LabeljunctionComponent, LabeljunctionDetailComponent, LabeljunctionUpdateComponent, LabeljunctionDeleteDialogComponent],
  entryComponents: [LabeljunctionDeleteDialogComponent]
})
export class GestionLabeljunctionModule {}
