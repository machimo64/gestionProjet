import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ILabeljunction, Labeljunction } from 'app/shared/model/labeljunction.model';
import { LabeljunctionService } from './labeljunction.service';
import { LabeljunctionComponent } from './labeljunction.component';
import { LabeljunctionDetailComponent } from './labeljunction-detail.component';
import { LabeljunctionUpdateComponent } from './labeljunction-update.component';

@Injectable({ providedIn: 'root' })
export class LabeljunctionResolve implements Resolve<ILabeljunction> {
  constructor(private service: LabeljunctionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILabeljunction> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((labeljunction: HttpResponse<Labeljunction>) => {
          if (labeljunction.body) {
            return of(labeljunction.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Labeljunction());
  }
}

export const labeljunctionRoute: Routes = [
  {
    path: '',
    component: LabeljunctionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestionApp.labeljunction.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: LabeljunctionDetailComponent,
    resolve: {
      labeljunction: LabeljunctionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestionApp.labeljunction.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: LabeljunctionUpdateComponent,
    resolve: {
      labeljunction: LabeljunctionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestionApp.labeljunction.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: LabeljunctionUpdateComponent,
    resolve: {
      labeljunction: LabeljunctionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestionApp.labeljunction.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
