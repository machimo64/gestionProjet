import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILabeljunction } from 'app/shared/model/labeljunction.model';
import { LabeljunctionService } from './labeljunction.service';

@Component({
  templateUrl: './labeljunction-delete-dialog.component.html'
})
export class LabeljunctionDeleteDialogComponent {
  labeljunction?: ILabeljunction;

  constructor(
    protected labeljunctionService: LabeljunctionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.labeljunctionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('labeljunctionListModification');
      this.activeModal.close();
    });
  }
}
