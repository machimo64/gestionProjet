import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILabeljunction } from 'app/shared/model/labeljunction.model';
import { LabeljunctionService } from './labeljunction.service';
import { LabeljunctionDeleteDialogComponent } from './labeljunction-delete-dialog.component';

@Component({
  selector: 'jhi-labeljunction',
  templateUrl: './labeljunction.component.html'
})
export class LabeljunctionComponent implements OnInit, OnDestroy {
  labeljunctions?: ILabeljunction[];
  eventSubscriber?: Subscription;

  constructor(
    protected labeljunctionService: LabeljunctionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.labeljunctionService.query().subscribe((res: HttpResponse<ILabeljunction[]>) => (this.labeljunctions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInLabeljunctions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ILabeljunction): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInLabeljunctions(): void {
    this.eventSubscriber = this.eventManager.subscribe('labeljunctionListModification', () => this.loadAll());
  }

  delete(labeljunction: ILabeljunction): void {
    const modalRef = this.modalService.open(LabeljunctionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.labeljunction = labeljunction;
  }
}
