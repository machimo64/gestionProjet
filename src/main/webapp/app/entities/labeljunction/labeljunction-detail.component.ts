import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILabeljunction } from 'app/shared/model/labeljunction.model';

@Component({
  selector: 'jhi-labeljunction-detail',
  templateUrl: './labeljunction-detail.component.html'
})
export class LabeljunctionDetailComponent implements OnInit {
  labeljunction: ILabeljunction | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ labeljunction }) => (this.labeljunction = labeljunction));
  }

  previousState(): void {
    window.history.back();
  }
}
