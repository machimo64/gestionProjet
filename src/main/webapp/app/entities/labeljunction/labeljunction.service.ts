import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ILabeljunction } from 'app/shared/model/labeljunction.model';

type EntityResponseType = HttpResponse<ILabeljunction>;
type EntityArrayResponseType = HttpResponse<ILabeljunction[]>;

@Injectable({ providedIn: 'root' })
export class LabeljunctionService {
  public resourceUrl = SERVER_API_URL + 'api/labeljunctions';

  constructor(protected http: HttpClient) {}

  create(labeljunction: ILabeljunction): Observable<EntityResponseType> {
    return this.http.post<ILabeljunction>(this.resourceUrl, labeljunction, { observe: 'response' });
  }

  update(labeljunction: ILabeljunction): Observable<EntityResponseType> {
    return this.http.put<ILabeljunction>(this.resourceUrl, labeljunction, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILabeljunction>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILabeljunction[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
