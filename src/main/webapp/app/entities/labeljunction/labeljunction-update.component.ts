import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ILabeljunction, Labeljunction } from 'app/shared/model/labeljunction.model';
import { LabeljunctionService } from './labeljunction.service';
import { ILabel } from 'app/shared/model/label.model';
import { LabelService } from 'app/entities/label/label.service';
import { ITache } from 'app/shared/model/tache.model';
import { TacheService } from 'app/entities/tache/tache.service';

type SelectableEntity = ILabel | ITache;

@Component({
  selector: 'jhi-labeljunction-update',
  templateUrl: './labeljunction-update.component.html'
})
export class LabeljunctionUpdateComponent implements OnInit {
  isSaving = false;
  labels: ILabel[] = [];
  taches: ITache[] = [];

  editForm = this.fb.group({
    id: [],
    label: [],
    tache: []
  });

  constructor(
    protected labeljunctionService: LabeljunctionService,
    protected labelService: LabelService,
    protected tacheService: TacheService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ labeljunction }) => {
      this.updateForm(labeljunction);

      this.labelService.query().subscribe((res: HttpResponse<ILabel[]>) => (this.labels = res.body || []));

      this.tacheService.query().subscribe((res: HttpResponse<ITache[]>) => (this.taches = res.body || []));
    });
  }

  updateForm(labeljunction: ILabeljunction): void {
    this.editForm.patchValue({
      id: labeljunction.id,
      label: labeljunction.label,
      tache: labeljunction.tache
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const labeljunction = this.createFromForm();
    if (labeljunction.id !== undefined) {
      this.subscribeToSaveResponse(this.labeljunctionService.update(labeljunction));
    } else {
      this.subscribeToSaveResponse(this.labeljunctionService.create(labeljunction));
    }
  }

  private createFromForm(): ILabeljunction {
    return {
      ...new Labeljunction(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      tache: this.editForm.get(['tache'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILabeljunction>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
