import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITache } from 'app/shared/model/tache.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ParticipantTacheAddComponent } from 'app/entities/participant/participant-tache-add.component';
import { ParticipantService } from 'app/entities/participant/participant.service';
import { IParticipant } from 'app/shared/model/participant.model';
import { ParticipantDeleteDialogComponent } from 'app/entities/participant/participant-delete-dialog.component';
import { JhiEventManager } from 'ng-jhipster';
import { forkJoin, Subscription } from 'rxjs';
import { Role } from 'app/shared/model/enumerations/role.model';
import { MembresService } from 'app/entities/membres/membres.service';
import { CommentaireService } from 'app/entities/commentaire/commentaire.service';
import { Commentaire, ICommentaire } from 'app/shared/model/commentaire.model';
import { IUser } from 'app/core/user/user.model';
import { CommentaireDeleteDialogComponent } from 'app/entities/commentaire/commentaire-delete-dialog.component';
import * as moment from 'moment';
import { FichierService } from 'app/entities/fichier/fichier.service';
import { IMembres } from 'app/shared/model/membres.model';
import { Notification } from 'app/shared/model/notification.model';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { UserService } from 'app/core/user/user.service';
import { NotificationService } from 'app/entities/notification/notification.service';
import { IListe } from 'app/shared/model/liste.model';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import { Fichier, IFichier } from 'app/shared/model/fichier.model';
import { saveAs } from 'file-saver';
import { FichierDeleteDialogComponent } from 'app/entities/fichier/fichier-delete-dialog.component';
import * as $ from 'jquery';

@Component({
  selector: 'jhi-tache-detail',
  templateUrl: './tache-detail.component.html'
})
export class TacheDetailComponent implements OnInit {
  tache: ITache | null = null;
  user!: IUser;
  isMembre: boolean | undefined;
  participants: IParticipant[] = [];
  commentaires: ICommentaire[] = [];
  eventSubscriber?: Subscription;
  modif: boolean | undefined;
  fichiers: IFichier[] = [];
  selectedFile: File | null = null;
  membre: IMembres | null = null;
  progress: { percentage: number } = { percentage: 0 };
  fileByteArray: any = [];
  reader: FileReader = new FileReader();
  fileToDownload: IFichier | null = null;
  membres: IMembres[] = [];
  liste: IListe | null = null;
  typeNotification!: ITypeNotification;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected participantService: ParticipantService,
    protected membreService: MembresService,
    protected commentaireService: CommentaireService,
    protected fichierService: FichierService,
    protected typeNotificationService: TypeNotificationService,
    protected userService: UserService,
    protected membresService: MembresService,
    protected notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.isMembre = false;
    this.modif = false;
    this.activatedRoute.data.subscribe(({ tache }) => (this.tache = tache));
    this.loadAll();
    this.registerChangeInParticipant();
    this.registerChangeInFichier();
    this.checkMembre();
  }

  checkMembre(): void {
    if (this.tache?.liste) {
      this.membreService.queryIfMembre(this.tache.liste.projet!.id!).subscribe(resultat => {
        if (resultat.body && resultat.body.role === Role.MODIFIER && resultat.body.user) {
          this.isMembre = true;
          this.membre = resultat.body;
          this.user = resultat.body.user;
        }
      });
    }
  }

  loadAll(): void {
    // On récupére les participants et les commentaires de la tâche
    forkJoin([
      this.participantService.findByTache(this.tache!.id!),
      this.commentaireService.findByTache(this.tache!.id!),
      this.fichierService.findByTache(this.tache!.id!)
    ]).subscribe(resultat => {
      this.participants = resultat[0].body || [];
      this.commentaires = resultat[1].body || [];
      this.fichiers = resultat[2].body || [];
    });
  }

  previousState(): void {
    window.history.back();
  }

  addParticipant(idTache: number): void {
    const modalRef = this.modalService.open(ParticipantTacheAddComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.idTache = idTache;
  }

  deleteParticipant(participant: IParticipant): void {
    const modalRef = this.modalService.open(ParticipantDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.participant = participant;
  }

  deleteFichier(fichier: IFichier): void {
    const modalRef = this.modalService.open(FichierDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fichier = fichier;
  }

  registerChangeInFichier(): void {
    this.eventSubscriber = this.eventManager.subscribe('fichierListModification', () => this.loadAll());
  }

  registerChangeInParticipant(): void {
    this.eventSubscriber = this.eventManager.subscribe('participantListModification', () => this.loadAll());
  }

  addCommentaire(): void {
    console.log(this.tache);
    const com = document.getElementById('newCommentaire') as HTMLTextAreaElement;
    if (this.user && this.tache) {
      const commentaire = new Commentaire(undefined, com.value, moment(), this.user, this.tache);
      this.commentaireService.create(commentaire).subscribe(result => {
        if (result.body) commentaire.id = result.body.id;
        this.commentaires.push(commentaire);
      });
      com.value = '';
      com.blur();
      this.loadAllNotifications();
    }
  }

  loadAllNotifications(): void {
    forkJoin([
      this.membresService.queryByProjet(this.tache!.liste!.projet!.id!),
      this.userService.getCurrentUser(),
      this.typeNotificationService.findByNom('Commentaire')
    ]).subscribe(resultat => {
      this.membres = resultat[0].body || [];
      this.user = resultat[1] || null;
      console.log(this.user);
      this.typeNotification = resultat[2].body!;
      console.log(resultat[2].body!);
      this.createNotification();
    });
  }
  createNotification(): void {
    this.membres.forEach(membre => {
      if (JSON.stringify(this.user) !== JSON.stringify(membre.user)) {
        const notification = new Notification(
          undefined,
          moment(),
          this.user,
          membre.user,
          this.typeNotification,
          this.tache!,
          undefined,
          undefined,
          membre
        );
        this.notificationService.create(notification).subscribe(resultat => console.log(resultat));
      }
    });
  }

  modifierCom(idCom: number): void {
    console.log('test');
    const id = '' + idCom;
    const p = document.getElementById(id + 'p');
    const inp = document.getElementById(id + 'in');
    const btM = document.getElementById(id + 'btm') as HTMLButtonElement;
    const btS = document.getElementById(id + 'bts') as HTMLButtonElement;
    if (p) p.hidden = true;
    if (inp) inp.hidden = false;
    if (btM) btM.hidden = true;
    if (btS) btS.hidden = true;
  }

  confirmModif(commentaire: ICommentaire): void {
    const inp = document.getElementById(commentaire.id + 'in') as HTMLTextAreaElement;
    const p = document.getElementById(commentaire.id + 'p') as HTMLParagraphElement;
    const btM = document.getElementById(commentaire.id + 'btm') as HTMLButtonElement;
    const btS = document.getElementById(commentaire.id + 'bts') as HTMLButtonElement;
    if (inp && p) commentaire.contenu = inp.value;
    p.textContent = inp.value;
    p.hidden = false;
    inp.hidden = true;
    btM.hidden = false;
    btS.hidden = false;
    this.commentaireService.update(commentaire).subscribe();
  }

  deleteCommentaire(commentaire: ICommentaire): void {
    const modalRef = this.modalService.open(CommentaireDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.commentaire = commentaire;
  }

  compareUsers(user: IUser, user2: IUser): boolean {
    return JSON.stringify(user) === JSON.stringify(user2);
  }

  selectFile(event: any): void {
    this.selectedFile = event.target.files[0];
  }

  // Fonction permettant d'upload un fichier vers la base
  uploadFile(): void {
    console.log(this.selectedFile);
    this.reader.readAsArrayBuffer(this.selectedFile!);
    this.reader.onloadend = evt => {
      if (evt.target?.readyState === FileReader.DONE) {
        const arrayBuffer = evt.target.result,
          array = new Uint8Array(arrayBuffer as ArrayBuffer);
        array.forEach((a: string | number) => this.fileByteArray.push(array[a]));
        const fichier: Fichier = new Fichier(
          undefined,
          this.selectedFile?.type,
          this.fileByteArray,
          this.selectedFile?.name,
          this.membre!,
          this.tache!
        );
        console.log(fichier);
        this.fichierService.create(fichier).subscribe(() => {
          alert('Le fichier a bien été enregistré');
          this.eventManager.broadcast('fichierListModification');
          this.selectedFile = null;
          $('#customFile').val('');
        });
      }
    };
  }

  download(idFichier: number): void {
    this.fichierService.find(idFichier).subscribe(fichier => {
      this.fileToDownload = fichier.body;
      console.log(this.fileToDownload);
      const file = new Blob([this.fileToDownload?.fichier], { type: this.fileToDownload?.fichierContentType });
      saveAs(file, this.fileToDownload?.nom);
    });
  }
}
