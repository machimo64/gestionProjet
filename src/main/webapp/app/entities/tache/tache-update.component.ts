import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';

import { ITache, Tache } from 'app/shared/model/tache.model';
import { TacheService } from './tache.service';
import { IListe } from 'app/shared/model/liste.model';
import { ListeService } from 'app/entities/liste/liste.service';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { UserService } from 'app/core/user/user.service';
import { MembresService } from 'app/entities/membres/membres.service';
import { IMembres } from 'app/shared/model/membres.model';
import { IUser } from 'app/core/user/user.model';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import { Notification } from 'app/shared/model/notification.model';
import { NotificationService } from 'app/entities/notification/notification.service';
import * as moment from 'moment';

@Component({
  selector: 'jhi-tache-update',
  templateUrl: './tache-update.component.html'
})
export class TacheUpdateComponent implements OnInit {
  isSaving = false;
  listes: IListe[] = [];

  membres: IMembres[] = [];
  user!: IUser;
  typeNotification!: ITypeNotification;
  tache: ITache | undefined;

  editForm = this.fb.group({
    id: [],
    titre: [null, [Validators.required]],
    dateDebut: [null, [Validators.required]],
    dateFin: [null, [Validators.required]],
    description: [null, [Validators.required]],
    etat: [],
    visible: [],
    poids: [],
    categorie: [],
    position: [null, [Validators.required]],
    liste: [null, Validators.required]
  });

  constructor(
    protected tacheService: TacheService,
    protected listeService: ListeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected typeNotificationService: TypeNotificationService,
    protected userService: UserService,
    protected membresService: MembresService,
    protected notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tache }) => {
      this.updateForm(tache);

      this.listeService.query().subscribe((res: HttpResponse<IListe[]>) => (this.listes = res.body || []));

      const fieldListe = document.getElementById('field_liste');
      if (fieldListe) fieldListe.style.visibility = 'hidden';
    });
  }

  updateForm(tache: ITache): void {
    this.editForm.patchValue({
      id: tache.id,
      titre: tache.titre,
      dateDebut: tache.dateDebut,
      dateFin: tache.dateFin,
      description: tache.description,
      etat: tache.etat,
      visible: tache.visible,
      poids: tache.poids,
      categorie: tache.categorie,
      position: tache.position,
      liste: tache.liste
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tache = this.createFromForm();
    this.tache = tache;
    if (tache.id !== undefined) {
      if (this.tache && this.tache.liste) this.loadNotification();
      this.subscribeToSaveResponse(this.tacheService.update(tache));
    } else {
      if (this.tache && this.tache.liste) this.loadNotification();
      this.subscribeToSaveResponse(this.tacheService.create(tache));
    }
  }

  private createFromForm(): ITache {
    return {
      ...new Tache(),
      id: this.editForm.get(['id'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      dateDebut: this.editForm.get(['dateDebut'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      description: this.editForm.get(['description'])!.value,
      etat: this.editForm.get(['etat'])!.value,
      visible: this.editForm.get(['visible'])!.value,
      poids: this.editForm.get(['poids'])!.value,
      categorie: this.editForm.get(['categorie'])!.value,
      position: this.editForm.get(['position'])!.value,
      liste: this.editForm.get(['liste'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITache>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IListe): any {
    return item.id;
  }

  loadNotification(): void {
    forkJoin([
      this.membresService.queryByProjet(this.tache!.liste!.projet!.id!),
      this.userService.getCurrentUser(),
      this.typeNotificationService.findByNom('Tache modifiée')
    ]).subscribe(resultat => {
      this.membres = resultat[0].body || [];
      console.log(this.membres);
      this.user = resultat[1] || null;
      console.log(this.user);
      this.typeNotification = resultat[2].body!;
      console.log(resultat[2].body!);
      this.createNotification();
    });
  }

  createNotification(): void {
    this.membres.forEach(membre => {
      if (JSON.stringify(this.user) !== JSON.stringify(membre.user)) {
        const notification = new Notification(undefined, moment(), this.user, membre.user, this.typeNotification, this.tache);
        this.notificationService.create(notification).subscribe(resultat => console.log(resultat));
      }
    });
  }
}
