import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IListe } from 'app/shared/model/liste.model';
import { ListeService } from './liste.service';
import { forkJoin } from 'rxjs';
import { Notification } from 'app/shared/model/notification.model';
import { IMembres } from 'app/shared/model/membres.model';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import { IUser } from 'app/core/user/user.model';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { UserService } from 'app/core/user/user.service';
import { MembresService } from 'app/entities/membres/membres.service';
import { NotificationService } from 'app/entities/notification/notification.service';
import * as moment from 'moment';

@Component({
  templateUrl: './liste-delete-dialog.component.html'
})
export class ListeDeleteDialogComponent {
  liste!: IListe | null;

  membres: IMembres[] = [];
  typeNotification!: ITypeNotification;
  user!: IUser;

  constructor(
    protected listeService: ListeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected typeNotificationService: TypeNotificationService,
    protected userService: UserService,
    protected membresService: MembresService,
    protected notificationService: NotificationService
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.listeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tacheListModification');
      this.activeModal.close();
    });
  }

  loadNotification(): void {
    forkJoin([
      this.membresService.queryByProjet(this.liste!.projet!.id!),
      this.userService.getCurrentUser(),
      this.typeNotificationService.findByNom('Liste supprimée')
    ]).subscribe(resultat => {
      this.membres = resultat[0].body || [];
      this.user = resultat[1] || null;
      console.log(this.user);
      this.typeNotification = resultat[2].body!;
      console.log(resultat[2].body!);
      this.createNotification();
    });
  }

  createNotification(): void {
    this.membres.forEach(membre => {
      if (JSON.stringify(this.user) !== JSON.stringify(membre.user)) {
        const notification = new Notification(undefined, moment(), this.user, membre.user, this.typeNotification, undefined, this.liste!);
        this.notificationService.create(notification).subscribe(resultat => console.log(resultat));
      }
    });
  }

  confirmArchive(id: number): void {
    if (this.liste) {
      if (this.liste.visible === false) {
        this.listeService.delete(id).subscribe(() => {
          this.eventManager.broadcast('listeListModification');
          this.activeModal.close();
        });
      } else {
        this.liste.visible = false;
        this.listeService.delete(id);
        this.listeService.update(this.liste).subscribe(res => {
          this.liste = res.body;
          this.activeModal.close();
          this.loadNotification();
          this.eventManager.broadcast('listeListModification');
        });
      }
    }
  }
}
