import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';

import { ITache, Tache } from 'app/shared/model/tache.model';
import { TacheService } from '../tache/tache.service';
import { IListe } from 'app/shared/model/liste.model';
import { NotificationService } from 'app/entities/notification/notification.service';
import { TypeNotificationService } from 'app/entities/type-notification/type-notification.service';
import { UserService } from 'app/core/user/user.service';
import { MembresService } from 'app/entities/membres/membres.service';
import { IMembres } from 'app/shared/model/membres.model';
import { Notification } from 'app/shared/model/notification.model';
import { IUser } from 'app/core/user/user.model';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import * as moment from 'moment';

@Component({
  selector: 'jhi-tache-update',
  templateUrl: './liste-new-tache.component.html'
})
export class ListeNewTacheComponent implements OnInit {
  isSaving = false;

  liste: IListe | null = null;
  membres: IMembres[] = [];
  user!: IUser;
  typeNotification!: ITypeNotification;
  tache!: ITache | null;

  editForm = this.fb.group({
    titre: [null, [Validators.required]],
    dateDebut: [null, [Validators.required]],
    dateFin: [null, [Validators.required]],
    description: [null, [Validators.required]],
    etat: [],
    poids: [],
    categorie: [],
    position: [null, Validators.required],
    liste: [null, Validators.required]
  });

  constructor(
    protected tacheService: TacheService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected typeNotificationService: TypeNotificationService,
    protected userService: UserService,
    protected membresService: MembresService,
    protected notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    // On récupére la liste
    this.activatedRoute.data.subscribe(({ liste }) => {
      this.liste = liste;
    });
    this.editForm.patchValue({
      liste: this.liste
    });
    this.editForm.patchValue({ position: 0 });
    this.loadAll();
  }

  previousState(): void {
    window.history.back();
  }

  loadAll(): void {
    forkJoin([
      this.membresService.queryByProjet(this.liste!.projet!.id!),
      this.userService.getCurrentUser(),
      this.typeNotificationService.findByNom('Tache créée')
    ]).subscribe(resultat => {
      this.membres = resultat[0].body || [];
      this.user = resultat[1] || null;
      this.typeNotification = resultat[2].body!;
    });
  }

  save(): void {
    this.isSaving = true;
    const tache = this.createFromForm();
    if (tache.id !== undefined) {
      this.subscribeToSaveResponse(this.tacheService.update(tache));
    } else {
      this.subscribeToSaveResponse(this.tacheService.create(tache));
    }
  }

  createNotification(): void {
    this.membres.forEach(membre => {
      if (JSON.stringify(this.user) !== JSON.stringify(membre.user)) {
        const notification = new Notification(undefined, moment(), this.user, membre.user, this.typeNotification, this.tache!);
        this.notificationService.create(notification).subscribe(resultat => console.log(resultat));
      }
    });
  }

  private createFromForm(): ITache {
    return {
      ...new Tache(),
      titre: this.editForm.get(['titre'])!.value,
      dateDebut: this.editForm.get(['dateDebut'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      description: this.editForm.get(['description'])!.value,
      etat: this.editForm.get(['etat'])!.value,
      poids: this.editForm.get(['poids'])!.value,
      categorie: this.editForm.get(['categorie'])!.value,
      liste: this.editForm.get(['liste'])!.value,
      position: this.editForm.get(['position'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITache>>): void {
    result.subscribe(res => {
      this.tache = res.body;
      this.onSaveSuccess();
    });
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
    this.createNotification();
  }

  trackById(index: number, item: IListe): any {
    return item.id;
  }
}
