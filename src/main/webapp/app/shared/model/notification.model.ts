import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { ITypeNotification } from 'app/shared/model/type-notification.model';
import { ITache } from 'app/shared/model/tache.model';
import { IListe } from 'app/shared/model/liste.model';
import { IProjet } from 'app/shared/model/projet.model';
import { IMembres } from 'app/shared/model/membres.model';

export interface INotification {
  id?: number;
  dateHeure?: Moment;
  emetteur?: IUser;
  destinataire?: IUser;
  typeNotification?: ITypeNotification;
  tache?: ITache;
  liste?: IListe;
  projet?: IProjet;
  membres?: IMembres;
}

export class Notification implements INotification {
  constructor(
    public id?: number,
    public dateHeure?: Moment,
    public emetteur?: IUser,
    public destinataire?: IUser,
    public typeNotification?: ITypeNotification,
    public tache?: ITache,
    public liste?: IListe,
    public projet?: IProjet,
    public membres?: IMembres
  ) {}
}
