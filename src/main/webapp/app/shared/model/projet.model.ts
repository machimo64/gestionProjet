import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IProjet {
  id?: number;
  titre?: string;
  dateDebut?: Moment;
  dateFin?: Moment;
  description?: string;
  modele?: string;
  etat?: boolean;
  visible?: boolean;
  user?: IUser;
}

export class Projet implements IProjet {
  constructor(
    public id?: number,
    public titre?: string,
    public dateDebut?: Moment,
    public dateFin?: Moment,
    public description?: string,
    public modele?: string,
    public etat?: boolean,
    public visible?: boolean,
    public user?: IUser
  ) {
    this.etat = this.etat || false;
    this.visible = this.visible || true;
  }
}
