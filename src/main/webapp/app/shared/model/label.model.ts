import { IProjet } from 'app/shared/model/projet.model';

export interface ILabel {
  id?: number;
  name?: string;
  color?: string;
  projet?: IProjet;
}

export class Label implements ILabel {
  constructor(public id?: number, public name?: string, public color?: string, public projet?: IProjet) {}
}
