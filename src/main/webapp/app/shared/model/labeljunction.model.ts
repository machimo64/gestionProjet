import { ILabel } from 'app/shared/model/label.model';
import { ITache } from 'app/shared/model/tache.model';

export interface ILabeljunction {
  id?: number;
  label?: ILabel;
  tache?: ITache;
}

export class Labeljunction implements ILabeljunction {
  constructor(public id?: number, public label?: ILabel, public tache?: ITache) {}
}
