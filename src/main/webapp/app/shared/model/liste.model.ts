import { IProjet } from 'app/shared/model/projet.model';

export interface IListe {
  id?: number;
  titre?: string;
  position?: number;
  visible?: boolean;
  projet?: IProjet;
}

export class Liste implements IListe {
  constructor(public id?: number, public titre?: string, public position?: number, public visible?: boolean, public projet?: IProjet) {
    this.visible = this.visible || true;
  }
}
