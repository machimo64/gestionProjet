package com.alteca.gestion.service;

import com.alteca.gestion.domain.Labeljunction;
import com.alteca.gestion.repository.LabeljunctionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Labeljunction}.
 */
@Service
@Transactional
public class LabeljunctionService {

    private final Logger log = LoggerFactory.getLogger(LabeljunctionService.class);

    private final LabeljunctionRepository labeljunctionRepository;

    public LabeljunctionService(LabeljunctionRepository labeljunctionRepository) {
        this.labeljunctionRepository = labeljunctionRepository;
    }

    /**
     * Save a labeljunction.
     *
     * @param labeljunction the entity to save.
     * @return the persisted entity.
     */
    public Labeljunction save(Labeljunction labeljunction) {
        log.debug("Request to save Labeljunction : {}", labeljunction);
        return labeljunctionRepository.save(labeljunction);
    }

    /**
     * Get all the labeljunctions.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Labeljunction> findAll() {
        log.debug("Request to get all Labeljunctions");
        return labeljunctionRepository.findAll();
    }


    /**
     * Get one labeljunction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Labeljunction> findOne(Long id) {
        log.debug("Request to get Labeljunction : {}", id);
        return labeljunctionRepository.findById(id);
    }

    /**
     * Delete the labeljunction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Labeljunction : {}", id);
        labeljunctionRepository.deleteById(id);
    }
}
