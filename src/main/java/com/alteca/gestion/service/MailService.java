package com.alteca.gestion.service;

import com.alteca.gestion.config.Constants;
import com.alteca.gestion.domain.*;

import com.alteca.gestion.repository.MembresRepository;
import io.github.jhipster.config.JHipsterProperties;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    private MembresRepository membresRepository;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine, MembresRepository membresRepository) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
        this.membresRepository = membresRepository;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        }  catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", to, e);
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        if (user.getEmail() == null) {
            log.debug("Email doesn't exist for user '{}'", user.getLogin());
            return;
        }
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }

    @Async
    public void taskCreateMail(Tache tache) {
        List<Membres> membreMail = this.membresRepository.findByProjet(tache.getListe().getProjet().getId());
        membreMail.forEach(mM -> {
            if(!mM.getUser().equals(tache.getListe().getProjet().getUser())) {
                User user = new User();
                user.setLangKey(Constants.DEFAULT_LANGUAGE);
                user.setLogin(mM.getUser().getLogin());
                user.setEmail(mM.getUser().getEmail());
                user.setId(tache.getId());
                user.setLastName(tache.getTitre()); //get task title
                user.setFirstName(tache.getListe().getProjet().getTitre()); //get project title

                log.debug("Sending task create email to '{}'", user.getEmail());
                sendEmailFromTemplate(user, "mail/taskCreateEmail", "email.createt.title");
            }
        });
    }

    @Async
    public void taskModifyMail(Tache tache) {
        List<Membres> membreMail = this.membresRepository.findByProjet(tache.getListe().getProjet().getId());
        membreMail.forEach(mM -> {
            if(!mM.getUser().equals(tache.getListe().getProjet().getUser())) {
                User user = new User();
                user.setLangKey(Constants.DEFAULT_LANGUAGE);
                user.setLogin(mM.getUser().getLogin());
                user.setEmail(mM.getUser().getEmail());
                user.setId(tache.getId());
                user.setLastName(tache.getTitre()); //get task title
                user.setFirstName(tache.getListe().getProjet().getTitre()); //get project title


                log.debug("Sending task create email to '{}'", mM.getUser().getEmail());
                sendEmailFromTemplate(user, "mail/taskModifyMail", "email.modifyt.title");
            }
            });
    }

    @Async
    public void assignTaskMail(Participant participant) {
        List<Membres> membreMail = this.membresRepository.findByProjet(participant.getTache().getListe().getProjet().getId());
        membreMail.forEach(mM -> {
            if (!mM.getUser().equals(participant.getTache().getListe().getProjet().getUser())) {
                User user = new User();
                user.setLangKey(Constants.DEFAULT_LANGUAGE);
                user.setLogin(mM.getUser().getLogin());
                user.setEmail(mM.getUser().getEmail());
                user.setId(participant.getTache().getId());
                user.setLastName(participant.getTache().getTitre()); //get task title
                user.setFirstName(participant.getTache().getListe().getProjet().getTitre()); //get project title

                log.debug("Sending task create email to '{}'", user.getEmail());
                sendEmailFromTemplate(user, "mail/assignTaskEmail", "email.assignt.title");
            }
        });
    }

    @Async
    public void commentMail(Commentaire commentaire) {
        List<Membres> membreMail = this.membresRepository.findByProjet(commentaire.getTache().getListe().getProjet().getId());
        membreMail.forEach(mM -> {
            if (!mM.getUser().equals(commentaire.getTache().getListe().getProjet().getUser())) {
                User user = new User();
                user.setLangKey(Constants.DEFAULT_LANGUAGE);
                user.setLogin(mM.getUser().getLogin());
                user.setEmail(mM.getUser().getEmail());
                user.setId(commentaire.getTache().getId());
                user.setLastName(commentaire.getTache().getTitre()); //get task title
                user.setFirstName(commentaire.getTache().getListe().getProjet().getTitre()); //get project title
                user.setCreatedBy(commentaire.getContenu()); //get comment

                log.debug("Sending task create email to '{}'", user.getEmail());
                sendEmailFromTemplate(user, "mail/commentEmail", "email.comment.title");
            }
        });
    }

    @Async
    public void listCreateMail(Liste liste) {
        List<Membres> membreMail = this.membresRepository.findByProjet(liste.getProjet().getId());
        membreMail.forEach(mM -> {
            if (!mM.getUser().equals(liste.getProjet().getUser())) {
                User user = new User();
                user.setLangKey(Constants.DEFAULT_LANGUAGE);
                user.setLogin(mM.getUser().getLogin());
                user.setEmail(mM.getUser().getEmail());
                user.setId(liste.getProjet().getId()); //get id of the project
                user.setLastName(liste.getTitre()); //get list title
                user.setFirstName(liste.getProjet().getTitre()); //get project title

                log.debug("Sending list create email to '{}'", user.getEmail());
                sendEmailFromTemplate(user, "mail/listeCreateMail", "email.createl.title");
            }
        });
    }

    @Async
    public void inviteMemberMail(Membres membres) {
        User user = new User();
        user.setLangKey(Constants.DEFAULT_LANGUAGE);
        user.setLogin(membres.getUser().getLogin());
        user.setFirstName(membres.getProjet().getTitre()); //get project title
        user.setId(membres.getProjet().getId()); //get id of the project
        user.setEmail(membres.getUser().getEmail());

        log.debug("Sending invitation project email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/inviteMemberEmail", "email.invitem.title");
    }
}
