package com.alteca.gestion.service;

import com.alteca.gestion.GestionApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import java.util.List;
import static org.springframework.ldap.query.LdapQueryBuilder.query;


@Service
public class LdapService {

    @Autowired
    private LdapTemplate ldapTemplate;

    private static final Logger log = LoggerFactory.getLogger(GestionApp.class);

    public List<String> getCN(String mail, String password){

        // Perform the authentication.
            return ldapTemplate.search(
                    query().where("mail").is(mail),
                    new AttributesMapper<String>() {
                        public String mapFromAttributes(Attributes attrs)
                            throws NamingException {
                            return (String) attrs.get("cn").get();
                        }
                    });
    }

    public List<String> getNickname(String mail, String password){

        // Perform the authentication.
        return ldapTemplate.search(
            query().where("mail").is(mail),
            new AttributesMapper<String>() {
                public String mapFromAttributes(Attributes attrs)
                    throws NamingException {
                    return (String) attrs.get("mailNickname").get();
                }
            });
    }



    public boolean authorize(String mail,String password) {
        try {
            log.info("Authorizing active directory ldap ....");
            Filter filter = new EqualsFilter("mail", mail);
            if(ldapTemplate.authenticate("", filter.encode(), password)){
                log.info("connected");
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return false;
    }


    /* public List<String> getAllCn(){
        List<String> agences = this.getAllAgences();
        List<String> cn = new ArrayList<String>();
        agences.forEach((agence) ->{
            ldapTemplate.search(
                query().base().where("cn").is("organizationalUnit"),
                new AttributesMapper<String>() {
                    public String mapFromAttributes(Attributes attrs)
                        throws NamingException {
                        return (String) attrs.get("OU").get();
                    }
                });
        });
    }*/



}
