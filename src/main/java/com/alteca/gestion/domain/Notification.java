package com.alteca.gestion.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Notification.
 */
@Entity
@Table(name = "notification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "date_heure", nullable = false)
    private ZonedDateTime dateHeure;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private User emetteur;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private User destinataire;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private TypeNotification typeNotification;

    @ManyToOne
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private Tache tache;

    @ManyToOne
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private Liste liste;

    @ManyToOne
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private Projet projet;

    @ManyToOne
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private Membres membres;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateHeure() {
        return dateHeure;
    }

    public Notification dateHeure(ZonedDateTime dateHeure) {
        this.dateHeure = dateHeure;
        return this;
    }

    public void setDateHeure(ZonedDateTime dateHeure) {
        this.dateHeure = dateHeure;
    }

    public User getEmetteur() {
        return emetteur;
    }

    public Notification emetteur(User user) {
        this.emetteur = user;
        return this;
    }

    public void setEmetteur(User user) {
        this.emetteur = user;
    }

    public User getDestinataire() {
        return destinataire;
    }

    public Notification destinataire(User user) {
        this.destinataire = user;
        return this;
    }

    public void setDestinataire(User user) {
        this.destinataire = user;
    }

    public TypeNotification getTypeNotification() {
        return typeNotification;
    }

    public Notification typeNotification(TypeNotification typeNotification) {
        this.typeNotification = typeNotification;
        return this;
    }

    public void setTypeNotification(TypeNotification typeNotification) {
        this.typeNotification = typeNotification;
    }

    public Tache getTache() {
        return tache;
    }

    public Notification tache(Tache tache) {
        this.tache = tache;
        return this;
    }

    public void setTache(Tache tache) {
        this.tache = tache;
    }

    public Liste getListe() {
        return liste;
    }

    public Notification liste(Liste liste) {
        this.liste = liste;
        return this;
    }

    public void setListe(Liste liste) {
        this.liste = liste;
    }

    public Projet getProjet() {
        return projet;
    }

    public Notification projet(Projet projet) {
        this.projet = projet;
        return this;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public Membres getMembres() {
        return membres;
    }

    public Notification membres(Membres membres) {
        this.membres = membres;
        return this;
    }

    public void setMembres(Membres membres) {
        this.membres = membres;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        return id != null && id.equals(((Notification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Notification{" +
            "id=" + getId() +
            ", dateHeure='" + getDateHeure() + "'" +
            "}";
    }
}
