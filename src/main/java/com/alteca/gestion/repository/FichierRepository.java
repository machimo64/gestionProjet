package com.alteca.gestion.repository;

import com.alteca.gestion.domain.Fichier;

import com.alteca.gestion.domain.Participant;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Fichier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FichierRepository extends JpaRepository<Fichier, Long> {

    @Query("select fichier from Fichier fichier where fichier.tache.id = :idTache")
    List<Fichier> findByTache(@Param("idTache") Long idTache);
}
