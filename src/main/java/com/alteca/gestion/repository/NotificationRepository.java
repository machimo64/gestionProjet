package com.alteca.gestion.repository;

import com.alteca.gestion.domain.Notification;

import com.alteca.gestion.domain.Tache;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    @Query("select notification from Notification notification where notification.emetteur.login = ?#{principal.username}")
    List<Notification> findByEmetteurIsCurrentUser();

    @Query("select notification from Notification notification where notification.destinataire.login = ?#{principal.username}")
    List<Notification> findByDestinataireIsCurrentUser();

    @Query("select notification from Notification notification where notification.typeNotification.nom = 'Tache supprimée'")
    List<Notification> findTacheDeleteNotification();

    @Query("select notification from Notification notification where notification.typeNotification.nom = 'Liste supprimée'")
    List<Notification> findListeDeleteNotification();

    @Query("delete from Tache tache where tache.id = :id AND tache.visible=false")
    List<Tache> deleteTache(@Param("id") Long id);

}
