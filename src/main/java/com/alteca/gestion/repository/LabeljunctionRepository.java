package com.alteca.gestion.repository;

import com.alteca.gestion.domain.Labeljunction;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Labeljunction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LabeljunctionRepository extends JpaRepository<Labeljunction, Long> {
}
