package com.alteca.gestion.repository;

import com.alteca.gestion.domain.Projet;
import com.alteca.gestion.domain.TypeNotification;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TypeNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeNotificationRepository extends JpaRepository<TypeNotification, Long> {

    // @Query("select typeNotification from TypeNotification typeNotification where typeNotification.nom = :nomTypeNotification")
    Optional<TypeNotification> findByNom(String nom);

}
