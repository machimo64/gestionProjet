package com.alteca.gestion.web.rest;

import com.alteca.gestion.domain.Labeljunction;
import com.alteca.gestion.service.LabeljunctionService;
import com.alteca.gestion.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.alteca.gestion.domain.Labeljunction}.
 */
@RestController
@RequestMapping("/api")
public class LabeljunctionResource {

    private final Logger log = LoggerFactory.getLogger(LabeljunctionResource.class);

    private static final String ENTITY_NAME = "labeljunction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LabeljunctionService labeljunctionService;

    public LabeljunctionResource(LabeljunctionService labeljunctionService) {
        this.labeljunctionService = labeljunctionService;
    }

    /**
     * {@code POST  /labeljunctions} : Create a new labeljunction.
     *
     * @param labeljunction the labeljunction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new labeljunction, or with status {@code 400 (Bad Request)} if the labeljunction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/labeljunctions")
    public ResponseEntity<Labeljunction> createLabeljunction(@RequestBody Labeljunction labeljunction) throws URISyntaxException {
        log.debug("REST request to save Labeljunction : {}", labeljunction);
        if (labeljunction.getId() != null) {
            throw new BadRequestAlertException("A new labeljunction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Labeljunction result = labeljunctionService.save(labeljunction);
        return ResponseEntity.created(new URI("/api/labeljunctions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /labeljunctions} : Updates an existing labeljunction.
     *
     * @param labeljunction the labeljunction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated labeljunction,
     * or with status {@code 400 (Bad Request)} if the labeljunction is not valid,
     * or with status {@code 500 (Internal Server Error)} if the labeljunction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/labeljunctions")
    public ResponseEntity<Labeljunction> updateLabeljunction(@RequestBody Labeljunction labeljunction) throws URISyntaxException {
        log.debug("REST request to update Labeljunction : {}", labeljunction);
        if (labeljunction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Labeljunction result = labeljunctionService.save(labeljunction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, labeljunction.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /labeljunctions} : get all the labeljunctions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of labeljunctions in body.
     */
    @GetMapping("/labeljunctions")
    public List<Labeljunction> getAllLabeljunctions() {
        log.debug("REST request to get all Labeljunctions");
        return labeljunctionService.findAll();
    }

    /**
     * {@code GET  /labeljunctions/:id} : get the "id" labeljunction.
     *
     * @param id the id of the labeljunction to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the labeljunction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/labeljunctions/{id}")
    public ResponseEntity<Labeljunction> getLabeljunction(@PathVariable Long id) {
        log.debug("REST request to get Labeljunction : {}", id);
        Optional<Labeljunction> labeljunction = labeljunctionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(labeljunction);
    }

    /**
     * {@code DELETE  /labeljunctions/:id} : delete the "id" labeljunction.
     *
     * @param id the id of the labeljunction to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/labeljunctions/{id}")
    public ResponseEntity<Void> deleteLabeljunction(@PathVariable Long id) {
        log.debug("REST request to delete Labeljunction : {}", id);
        labeljunctionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
