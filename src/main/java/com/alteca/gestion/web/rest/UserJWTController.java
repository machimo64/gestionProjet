package com.alteca.gestion.web.rest;

import com.alteca.gestion.GestionApp;
import com.alteca.gestion.domain.Authority;
import com.alteca.gestion.domain.User;
import com.alteca.gestion.repository.UserRepository;
import com.alteca.gestion.security.jwt.JWTFilter;
import com.alteca.gestion.security.jwt.TokenProvider;
import com.alteca.gestion.service.LdapService;
import com.alteca.gestion.service.UserService;
import com.alteca.gestion.service.dto.UserDTO;
import com.alteca.gestion.web.rest.vm.LoginVM;

import com.alteca.gestion.web.rest.vm.ManagedUserVM;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private static final Logger log = LoggerFactory.getLogger(GestionApp.class);

    private final AuthenticationManagerBuilder authenticationManagerBuilder;


    private final UserRepository userRepository;

    private final UserService userService;

    @Autowired
    LdapService ldapService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder, UserRepository userRepository, UserService userService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
        // Si on se connecte au LDAP avec les identifiants, on vérifie si
            // l'adresse mail existe déjà, sinon on crée un compte
             if(ldapService.authorize(loginVM.getUsername(), loginVM.getPassword())){
                 if(!userRepository.findOneByEmailIgnoreCase(loginVM.getUsername()).isPresent()){
                     String[] nom = ldapService.getCN(loginVM.getUsername(), loginVM.getPassword()).get(0).split(" ");
                     ManagedUserVM user = new ManagedUserVM();
                     user.setLogin(ldapService.getNickname(loginVM.getUsername(), loginVM.getPassword()).get(0));
                     user.setFirstName(nom[1]);
                     user.setLastName(nom[0]);
                     user.setEmail(loginVM.getUsername());
                     user.setLangKey("fr");
                     userService.registerUser(user, loginVM.getPassword());
                 }
            }

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());


        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }
    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
