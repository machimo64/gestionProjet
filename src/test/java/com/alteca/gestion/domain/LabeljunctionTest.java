package com.alteca.gestion.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.alteca.gestion.web.rest.TestUtil;

public class LabeljunctionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Labeljunction.class);
        Labeljunction labeljunction1 = new Labeljunction();
        labeljunction1.setId(1L);
        Labeljunction labeljunction2 = new Labeljunction();
        labeljunction2.setId(labeljunction1.getId());
        assertThat(labeljunction1).isEqualTo(labeljunction2);
        labeljunction2.setId(2L);
        assertThat(labeljunction1).isNotEqualTo(labeljunction2);
        labeljunction1.setId(null);
        assertThat(labeljunction1).isNotEqualTo(labeljunction2);
    }
}
