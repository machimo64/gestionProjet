package com.alteca.gestion.web.rest;

import com.alteca.gestion.GestionApp;
import com.alteca.gestion.domain.Labeljunction;
import com.alteca.gestion.repository.LabeljunctionRepository;
import com.alteca.gestion.service.LabeljunctionService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LabeljunctionResource} REST controller.
 */
@SpringBootTest(classes = GestionApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LabeljunctionResourceIT {

    @Autowired
    private LabeljunctionRepository labeljunctionRepository;

    @Autowired
    private LabeljunctionService labeljunctionService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLabeljunctionMockMvc;

    private Labeljunction labeljunction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Labeljunction createEntity(EntityManager em) {
        Labeljunction labeljunction = new Labeljunction();
        return labeljunction;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Labeljunction createUpdatedEntity(EntityManager em) {
        Labeljunction labeljunction = new Labeljunction();
        return labeljunction;
    }

    @BeforeEach
    public void initTest() {
        labeljunction = createEntity(em);
    }

    @Test
    @Transactional
    public void createLabeljunction() throws Exception {
        int databaseSizeBeforeCreate = labeljunctionRepository.findAll().size();
        // Create the Labeljunction
        restLabeljunctionMockMvc.perform(post("/api/labeljunctions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labeljunction)))
            .andExpect(status().isCreated());

        // Validate the Labeljunction in the database
        List<Labeljunction> labeljunctionList = labeljunctionRepository.findAll();
        assertThat(labeljunctionList).hasSize(databaseSizeBeforeCreate + 1);
        Labeljunction testLabeljunction = labeljunctionList.get(labeljunctionList.size() - 1);
    }

    @Test
    @Transactional
    public void createLabeljunctionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = labeljunctionRepository.findAll().size();

        // Create the Labeljunction with an existing ID
        labeljunction.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLabeljunctionMockMvc.perform(post("/api/labeljunctions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labeljunction)))
            .andExpect(status().isBadRequest());

        // Validate the Labeljunction in the database
        List<Labeljunction> labeljunctionList = labeljunctionRepository.findAll();
        assertThat(labeljunctionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLabeljunctions() throws Exception {
        // Initialize the database
        labeljunctionRepository.saveAndFlush(labeljunction);

        // Get all the labeljunctionList
        restLabeljunctionMockMvc.perform(get("/api/labeljunctions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(labeljunction.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getLabeljunction() throws Exception {
        // Initialize the database
        labeljunctionRepository.saveAndFlush(labeljunction);

        // Get the labeljunction
        restLabeljunctionMockMvc.perform(get("/api/labeljunctions/{id}", labeljunction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(labeljunction.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingLabeljunction() throws Exception {
        // Get the labeljunction
        restLabeljunctionMockMvc.perform(get("/api/labeljunctions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLabeljunction() throws Exception {
        // Initialize the database
        labeljunctionService.save(labeljunction);

        int databaseSizeBeforeUpdate = labeljunctionRepository.findAll().size();

        // Update the labeljunction
        Labeljunction updatedLabeljunction = labeljunctionRepository.findById(labeljunction.getId()).get();
        // Disconnect from session so that the updates on updatedLabeljunction are not directly saved in db
        em.detach(updatedLabeljunction);

        restLabeljunctionMockMvc.perform(put("/api/labeljunctions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLabeljunction)))
            .andExpect(status().isOk());

        // Validate the Labeljunction in the database
        List<Labeljunction> labeljunctionList = labeljunctionRepository.findAll();
        assertThat(labeljunctionList).hasSize(databaseSizeBeforeUpdate);
        Labeljunction testLabeljunction = labeljunctionList.get(labeljunctionList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingLabeljunction() throws Exception {
        int databaseSizeBeforeUpdate = labeljunctionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLabeljunctionMockMvc.perform(put("/api/labeljunctions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labeljunction)))
            .andExpect(status().isBadRequest());

        // Validate the Labeljunction in the database
        List<Labeljunction> labeljunctionList = labeljunctionRepository.findAll();
        assertThat(labeljunctionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLabeljunction() throws Exception {
        // Initialize the database
        labeljunctionService.save(labeljunction);

        int databaseSizeBeforeDelete = labeljunctionRepository.findAll().size();

        // Delete the labeljunction
        restLabeljunctionMockMvc.perform(delete("/api/labeljunctions/{id}", labeljunction.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Labeljunction> labeljunctionList = labeljunctionRepository.findAll();
        assertThat(labeljunctionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
