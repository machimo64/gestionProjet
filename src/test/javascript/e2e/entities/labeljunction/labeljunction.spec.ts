import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { LabeljunctionComponentsPage, LabeljunctionDeleteDialog, LabeljunctionUpdatePage } from './labeljunction.page-object';

const expect = chai.expect;

describe('Labeljunction e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let labeljunctionComponentsPage: LabeljunctionComponentsPage;
  let labeljunctionUpdatePage: LabeljunctionUpdatePage;
  let labeljunctionDeleteDialog: LabeljunctionDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Labeljunctions', async () => {
    await navBarPage.goToEntity('labeljunction');
    labeljunctionComponentsPage = new LabeljunctionComponentsPage();
    await browser.wait(ec.visibilityOf(labeljunctionComponentsPage.title), 5000);
    expect(await labeljunctionComponentsPage.getTitle()).to.eq('gestionApp.labeljunction.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(labeljunctionComponentsPage.entities), ec.visibilityOf(labeljunctionComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Labeljunction page', async () => {
    await labeljunctionComponentsPage.clickOnCreateButton();
    labeljunctionUpdatePage = new LabeljunctionUpdatePage();
    expect(await labeljunctionUpdatePage.getPageTitle()).to.eq('gestionApp.labeljunction.home.createOrEditLabel');
    await labeljunctionUpdatePage.cancel();
  });

  it('should create and save Labeljunctions', async () => {
    const nbButtonsBeforeCreate = await labeljunctionComponentsPage.countDeleteButtons();

    await labeljunctionComponentsPage.clickOnCreateButton();

    await promise.all([labeljunctionUpdatePage.labelSelectLastOption(), labeljunctionUpdatePage.tacheSelectLastOption()]);

    await labeljunctionUpdatePage.save();
    expect(await labeljunctionUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await labeljunctionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Labeljunction', async () => {
    const nbButtonsBeforeDelete = await labeljunctionComponentsPage.countDeleteButtons();
    await labeljunctionComponentsPage.clickOnLastDeleteButton();

    labeljunctionDeleteDialog = new LabeljunctionDeleteDialog();
    expect(await labeljunctionDeleteDialog.getDialogTitle()).to.eq('gestionApp.labeljunction.delete.question');
    await labeljunctionDeleteDialog.clickOnConfirmButton();

    expect(await labeljunctionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
