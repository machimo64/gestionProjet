import { element, by, ElementFinder } from 'protractor';

export class LabeljunctionComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-labeljunction div table .btn-danger'));
  title = element.all(by.css('jhi-labeljunction div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class LabeljunctionUpdatePage {
  pageTitle = element(by.id('jhi-labeljunction-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  labelSelect = element(by.id('field_label'));
  tacheSelect = element(by.id('field_tache'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async labelSelectLastOption(): Promise<void> {
    await this.labelSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async labelSelectOption(option: string): Promise<void> {
    await this.labelSelect.sendKeys(option);
  }

  getLabelSelect(): ElementFinder {
    return this.labelSelect;
  }

  async getLabelSelectedOption(): Promise<string> {
    return await this.labelSelect.element(by.css('option:checked')).getText();
  }

  async tacheSelectLastOption(): Promise<void> {
    await this.tacheSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async tacheSelectOption(option: string): Promise<void> {
    await this.tacheSelect.sendKeys(option);
  }

  getTacheSelect(): ElementFinder {
    return this.tacheSelect;
  }

  async getTacheSelectedOption(): Promise<string> {
    return await this.tacheSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class LabeljunctionDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-labeljunction-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-labeljunction'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
