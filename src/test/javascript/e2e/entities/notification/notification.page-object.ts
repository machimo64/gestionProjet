import { element, by, ElementFinder } from 'protractor';

export class NotificationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-notification div table .btn-danger'));
  title = element.all(by.css('jhi-notification div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class NotificationUpdatePage {
  pageTitle = element(by.id('jhi-notification-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  dateHeureInput = element(by.id('field_dateHeure'));

  emetteurSelect = element(by.id('field_emetteur'));
  destinataireSelect = element(by.id('field_destinataire'));
  typeNotificationSelect = element(by.id('field_typeNotification'));
  tacheSelect = element(by.id('field_tache'));
  listeSelect = element(by.id('field_liste'));
  projetSelect = element(by.id('field_projet'));
  membresSelect = element(by.id('field_membres'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateHeureInput(dateHeure: string): Promise<void> {
    await this.dateHeureInput.sendKeys(dateHeure);
  }

  async getDateHeureInput(): Promise<string> {
    return await this.dateHeureInput.getAttribute('value');
  }

  async emetteurSelectLastOption(): Promise<void> {
    await this.emetteurSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async emetteurSelectOption(option: string): Promise<void> {
    await this.emetteurSelect.sendKeys(option);
  }

  getEmetteurSelect(): ElementFinder {
    return this.emetteurSelect;
  }

  async getEmetteurSelectedOption(): Promise<string> {
    return await this.emetteurSelect.element(by.css('option:checked')).getText();
  }

  async destinataireSelectLastOption(): Promise<void> {
    await this.destinataireSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async destinataireSelectOption(option: string): Promise<void> {
    await this.destinataireSelect.sendKeys(option);
  }

  getDestinataireSelect(): ElementFinder {
    return this.destinataireSelect;
  }

  async getDestinataireSelectedOption(): Promise<string> {
    return await this.destinataireSelect.element(by.css('option:checked')).getText();
  }

  async typeNotificationSelectLastOption(): Promise<void> {
    await this.typeNotificationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async typeNotificationSelectOption(option: string): Promise<void> {
    await this.typeNotificationSelect.sendKeys(option);
  }

  getTypeNotificationSelect(): ElementFinder {
    return this.typeNotificationSelect;
  }

  async getTypeNotificationSelectedOption(): Promise<string> {
    return await this.typeNotificationSelect.element(by.css('option:checked')).getText();
  }

  async tacheSelectLastOption(): Promise<void> {
    await this.tacheSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async tacheSelectOption(option: string): Promise<void> {
    await this.tacheSelect.sendKeys(option);
  }

  getTacheSelect(): ElementFinder {
    return this.tacheSelect;
  }

  async getTacheSelectedOption(): Promise<string> {
    return await this.tacheSelect.element(by.css('option:checked')).getText();
  }

  async listeSelectLastOption(): Promise<void> {
    await this.listeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async listeSelectOption(option: string): Promise<void> {
    await this.listeSelect.sendKeys(option);
  }

  getListeSelect(): ElementFinder {
    return this.listeSelect;
  }

  async getListeSelectedOption(): Promise<string> {
    return await this.listeSelect.element(by.css('option:checked')).getText();
  }

  async projetSelectLastOption(): Promise<void> {
    await this.projetSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async projetSelectOption(option: string): Promise<void> {
    await this.projetSelect.sendKeys(option);
  }

  getProjetSelect(): ElementFinder {
    return this.projetSelect;
  }

  async getProjetSelectedOption(): Promise<string> {
    return await this.projetSelect.element(by.css('option:checked')).getText();
  }

  async membresSelectLastOption(): Promise<void> {
    await this.membresSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async membresSelectOption(option: string): Promise<void> {
    await this.membresSelect.sendKeys(option);
  }

  getMembresSelect(): ElementFinder {
    return this.membresSelect;
  }

  async getMembresSelectedOption(): Promise<string> {
    return await this.membresSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class NotificationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-notification-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-notification'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
