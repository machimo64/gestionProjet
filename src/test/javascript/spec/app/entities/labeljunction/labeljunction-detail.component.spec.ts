import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestionTestModule } from '../../../test.module';
import { LabeljunctionDetailComponent } from 'app/entities/labeljunction/labeljunction-detail.component';
import { Labeljunction } from 'app/shared/model/labeljunction.model';

describe('Component Tests', () => {
  describe('Labeljunction Management Detail Component', () => {
    let comp: LabeljunctionDetailComponent;
    let fixture: ComponentFixture<LabeljunctionDetailComponent>;
    const route = ({ data: of({ labeljunction: new Labeljunction(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestionTestModule],
        declarations: [LabeljunctionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(LabeljunctionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LabeljunctionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load labeljunction on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.labeljunction).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
