import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GestionTestModule } from '../../../test.module';
import { LabeljunctionComponent } from 'app/entities/labeljunction/labeljunction.component';
import { LabeljunctionService } from 'app/entities/labeljunction/labeljunction.service';
import { Labeljunction } from 'app/shared/model/labeljunction.model';

describe('Component Tests', () => {
  describe('Labeljunction Management Component', () => {
    let comp: LabeljunctionComponent;
    let fixture: ComponentFixture<LabeljunctionComponent>;
    let service: LabeljunctionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestionTestModule],
        declarations: [LabeljunctionComponent]
      })
        .overrideTemplate(LabeljunctionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LabeljunctionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LabeljunctionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Labeljunction(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.labeljunctions && comp.labeljunctions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
