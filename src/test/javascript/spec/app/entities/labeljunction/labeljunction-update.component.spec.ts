import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestionTestModule } from '../../../test.module';
import { LabeljunctionUpdateComponent } from 'app/entities/labeljunction/labeljunction-update.component';
import { LabeljunctionService } from 'app/entities/labeljunction/labeljunction.service';
import { Labeljunction } from 'app/shared/model/labeljunction.model';

describe('Component Tests', () => {
  describe('Labeljunction Management Update Component', () => {
    let comp: LabeljunctionUpdateComponent;
    let fixture: ComponentFixture<LabeljunctionUpdateComponent>;
    let service: LabeljunctionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestionTestModule],
        declarations: [LabeljunctionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(LabeljunctionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LabeljunctionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LabeljunctionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Labeljunction(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Labeljunction();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
